import requests
import re
from pprint import pprint


def search(search_str):
    # поиск по апи
    search_url = 'https://ru.wikipedia.org/w/api.php?action=opensearch&limit=1&namespace=0&format=json'
    search_url += '&search=%(search_str)s' % {'search_str': search_str}
    
    r = requests.get(search_url)
    
    res_data = None
    
    if r.status_code == 200:
        try:
            res_data = r.json()
        except:
            pass
    
    if res_data and res_data[1]:
        text = res_data[2][0]
        url = res_data[3][0]
        top3_relay = get_sub_urls(url)
    else:
        text = None
        url = None
        top3_relay = None
    
    return {
        'text': text,
        'url': url,
        'top3_relay': top3_relay
    }


def get_sub_urls(url):
    # парсинг ссылок из исходника страницы
    top3_relay = []
    
    r = requests.get(url)
    if r.status_code == 200:
        result = re.search('id="См._также"(.*?)<ul>(.*?)<\/ul>', r.text, (re.MULTILINE | re.DOTALL))

        if result:
            urls_context = result.group(2)
            founded_urls = re.findall('href=\"(.*?)\".*?title=\"(.*?)\"', urls_context)
            
            for url_info in founded_urls[:3]:
                top3_relay.append({
                    'url': 'https://ru.wikipedia.org' + url_info[0] if url_info[0][:4] != 'http' else url_info[0],
                    'text': url_info[1],
                })
    
    return top3_relay
