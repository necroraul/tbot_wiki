from common.MongoDB import get_mongodb
from time import time
from pprint import pprint


def save_history(chat_id, message_id, value):
    mongodb = get_mongodb()
    # удаляем все записи из этого чата и не из текущего сообщения 
    mongodb.db['chat_history'].remove({"chat_id": chat_id, "message_id": {"$ne": message_id}}, multi=True)
    
    # очищаем значения больше текущего если произошел новый переход
    last_cur_position = mongodb.db['chat_history'].find_one({
        "chat_id": chat_id,
        "message_id": message_id,
        'current_position': True
    })
    if last_cur_position:
        mongodb.db['chat_history'].remove({
            "chat_id": chat_id,
            "message_id": message_id,
            "order_time": {"$gt": last_cur_position['order_time']}
        }, multi=True)
    
    # во всех случаях перед инсертом нового значения обновляем метки у всех значений
    mongodb.db['chat_history'].update({
            "chat_id": chat_id,
            "message_id": message_id
        },
        {
            '$set': {'current_position': False}
        },
        multi=True
    )
    
    # вставка свежего значения
    val_id = mongodb.db['chat_history'].insert({
        'chat_id': chat_id,
        'message_id': message_id,
        'value': value,
        'order_time': int(time()),
        'current_position': True
    })
    

def get_history_position(chat_id, message_id):
    mongodb = get_mongodb()
    
    history_positions = {
        'previos': None,
        'current': None,
        'next': None,
    }
    
    history_items = mongodb.db['chat_history'].find({
        "chat_id": chat_id,
        "message_id": message_id
    }).sort('order_time', 1)
    
    for item in history_items:
        if item['current_position']:
            history_positions['current'] = item
            
        elif not history_positions['current']:
            history_positions['previos'] = item
            
        elif history_positions['current'] and not history_positions['next']:
            history_positions['next'] = item
    
    return history_positions


def go_to_item(item):
    #
    mongodb = get_mongodb()
    mongodb.db['chat_history'].update({
            "chat_id": item['chat_id'],
            "message_id": item['message_id'],
        },
        {
            '$set': {'current_position': False}
        },
        multi=True
    )
    
    mongodb.db['chat_history'].update(
        { '_id': item['_id']},
        {
            '$set': {'current_position': True}
        },
    )
    