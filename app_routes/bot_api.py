import requests
import json
import traceback
from pprint import pprint
from app_routes import tbot_app
from flask import jsonify
from flask import request as flask_request

from common.ConfigData import conf
from actions.wiki import search
from actions.history import save_history, get_history_position, go_to_item


@tbot_app.route('/botapi', methods=['POST'])
def botapi_answer():
    data = flask_request.get_json()

    if 'message' in data:
        chat_id = data['message']['chat']['id']
        input_text = data['message']['text']
        from_data = data['message']['from']
        message_id = data['message']['message_id']
        
        search_result = search(input_text)
        
        if search_result['text']:
            message = __main_msg_content(
                chat_id = chat_id,
                wiki_text = search_result['text'],
                wiki_url = search_result['url'],
                top3_relay = search_result['top3_relay']
            )
            
            has_history = True
        else:
            message = {
                'chat_id': chat_id,
                'text': 'По вашему запросу ничего не найдено. Попробуйте еще раз.'
            }
            # если ничего на найдено то лишний раз сохранять ничего не нужно
            has_history = False
        
        r = requests.post(
            conf.api_url % {
                'token': conf.api_token,
                'method': 'sendMessage'
            },
            data = message
        )
        if has_history and r.status_code == 200:
            try:
                res_data = r.json()
                save_history(chat_id, res_data['result']['message_id'], input_text)
            except:
                pprint(traceback.format_exc())
                pass
    
    
    elif 'callback_query' in data:
        
        if 'data' in data['callback_query']:
            
            if 'message' in data['callback_query']:
                chat_id = data['callback_query']['message']['chat']['id']
                message_id = data['callback_query']['message']['message_id']
                
                
                history_position = get_history_position(chat_id, message_id)
                
                if data['callback_query']['data'] in ['previos', 'next']:
                    # check chat id
                    if history_position[data['callback_query']['data']]:
                        
                        target_position = history_position[data['callback_query']['data']] 
                        
                        go_to_item(target_position)
                        
                        search_result = search(target_position['value'])
                        
                        new_history_position = get_history_position(chat_id, message_id)
                        
                        message = __main_msg_content(
                            chat_id = chat_id,
                            message_id = message_id,
                            wiki_text = search_result['text'],
                            wiki_url = search_result['url'],
                            top3_relay = search_result['top3_relay'],
                            history_position = new_history_position,
                        )
                        
                        r = requests.post(
                            conf.api_url % {
                                'token': conf.api_token,
                                'method': 'editMessageText'
                            },
                            data = message
                        )                     
                    
                else:
                    search_result = search(data['callback_query']['data'])
                    save_history(chat_id, message_id, data['callback_query']['data'])
                    
                    new_history_position = get_history_position(chat_id, message_id)
                    
                    message = __main_msg_content(
                        chat_id = chat_id,
                        message_id = message_id,
                        wiki_text = search_result['text'],
                        wiki_url = search_result['url'],
                        top3_relay = search_result['top3_relay'],
                        history_position = new_history_position,
                    )
                    
                    r = requests.post(
                        conf.api_url % {
                            'token': conf.api_token,
                            'method': 'editMessageText'
                        },
                        data = message
                    )                    

    else:
        pprint(data)
    
    return jsonify({'status': 'ok'})


def __main_msg_content(chat_id, wiki_text, wiki_url, top3_relay=[], message_id=None, history_position=None):
    
    buttons = []
    # buttons.append(__button_to_url('в википедию', wiki_url))
    
    for button_item in top3_relay:
        buttons.append([__button_to_action(button_item['text'], button_item['text'])])
    
    history_buttons = []
    if history_position:
        if history_position['previos']:
            history_buttons.append(__button_to_action('назад', 'previos'))
            
            
        if history_position['next']:
            history_buttons.append(__button_to_action('вперед', 'next'))
    
    if history_buttons:
      buttons.append(history_buttons)  
    
    # buttons.append([
    #     __button_to_action('назад', 'previos'),
    #     __button_to_action('вперед', 'next')
    # ]) 
    
    message = {
        'chat_id': chat_id,
        'text': wiki_text + "\n[" + wiki_url + "]",
        'reply_markup': json.dumps({'inline_keyboard': buttons}),
    }    
    
    if message_id:
        message['message_id'] = message_id
    
    return message


def __button_to_url(text, url):
    return {'text': text, 'url': url}


def __button_to_action(text, value):
    return {
        'text': text,
        'callback_data': value
    }
    
