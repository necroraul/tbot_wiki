
class ConfigData:
	
	def __init__(self):
		self.default_config = getattr(__import__('configs.default'), 'default')
		self.local_config = getattr(__import__('configs.local'), 'local')
		
		
	def __getattr__(self, name):
		
		attr_value = None
		
		if hasattr(self.local_config, name):
			attr_value = getattr(self.local_config, name)
			
		elif hasattr(self.default_config, name):
			attr_value = getattr(self.default_config, name)
		
		else:
			raise Exception('Wrong config attr name')
		
		return attr_value

	
conf = ConfigData()