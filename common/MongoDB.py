import pymongo
from pymongo.read_preferences import ReadPreference
from common.ConfigData import conf

class MongoDB(object):
	db = None
	
	def __init__(self, host='localhost', port=27017, db='', user='', password=''):
		connect_data = {
			'host': host,
			'port': port
		}
		
		connection = pymongo.MongoClient(**connect_data)
		
		self.db = connection[db]

		if user and password:
			self.db.authenticate(user or '', password or '')


mongodb = None

def get_mongodb():
	global mongodb
	
	if not mongodb:
		mongodb = MongoDB(db=conf.mongo_db, user=conf.mongo_user, password=conf.mongo_pass)
	
	return mongodb