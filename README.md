## Бот для поиска в wiki


## Структура файлов
```
./
|-- README.md
|-- actions               * работа с данными 
|   |-- __init__.py
|   |-- history.py            * модуль по работе с историей переходов
|   `-- wiki.py               * модуль по работе с получением данных из вики
|-- app_routes            * роуты для фласка
|   |-- __init__.py
|   `-- bot_api.py            * точка входа api (вебхук для бота)
|-- common
|   |-- ConfigData.py     * библиотека работы с конфигами
|   |-- MongoDB.py        * библиотека работы с монгодб
|   |-- __init__.py
|-- configs               * конфиги для приложения 
|   |-- __init__.py           * инициализация flask-приложения
|   |-- default.py            * основной конфиг
|   `-- local.py              * уточняющий конфиг (создаётся вручную на месте)
|-- deploy_configs        * системные конфиги для nginx и uwsgi
|   |-- local
|   |-- nginx.conf
|   |-- touch_reload
|   `-- uwsgi_vassals.ini
|-- tbot_app.py           * точка входа 
|-- tbot_register.py      * скрипт для регистрации апи-бота в телеграмм
`-- var                   * логи и т.д.
    |-- access.log
    |-- error.log
    |-- tbot_app_uwsgi.log 
    `-- tbot_app_uwsgi.pid
```
