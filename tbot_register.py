import requests
from common.ConfigData import conf

if __name__ == '__main__':
    r = requests.post(
        conf.api_url % {
            'token': conf.api_token,
            'method': 'setWebhook'
        },
        data = {
            'url': conf.tbot_api_url
        }
    )
    
    print(r.status_code)
    print(r.text)
        